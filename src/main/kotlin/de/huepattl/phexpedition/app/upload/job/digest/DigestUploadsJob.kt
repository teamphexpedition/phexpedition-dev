package de.huepattl.phexpedition.app.upload.job.digest

import de.huepattl.phexpedition.app.repository.*
import de.huepattl.phexpedition.app.upload.StagedUpload
import org.slf4j.LoggerFactory
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.batch.item.ItemProcessor
import org.springframework.batch.item.ItemReader
import org.springframework.batch.item.ItemWriter
import org.springframework.batch.item.database.builder.JpaPagingItemReaderBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.File
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.temporal.TemporalUnit
import javax.persistence.EntityManagerFactory


@Configuration
@EnableBatchProcessing
class DigestUploadsJobConfiguration(
        @Autowired val jobBuilderFactory: JobBuilderFactory,
        @Autowired val stepBuilderFactory: StepBuilderFactory,
        @Autowired val entityManagerFactory: EntityManagerFactory,
        @Autowired val photoRepository: PhotoRepository,
        @Value("\${phex.upload.directory}") val stage: String,
        @Value("\${phex.repo.directory}") val repo: String) {

    @Bean
    fun reader(): ItemReader<StagedUpload> {
        return JpaPagingItemReaderBuilder<StagedUpload>()
                .name("jpareader")
                .entityManagerFactory(entityManagerFactory)
                .pageSize(100)
                .queryString("select su from StagedUpload su where processed = false")
                .build()
    }

    @Bean
    fun processor(): ItemProcessor<StagedUpload, Photo> {
        return UploadProcessor(repo)
    }

    @Bean
    fun writer(): ItemWriter<Photo> {
        return PhotoWriter(photoRepository)
    }

    @Bean(name = ["importJob"])
    fun importJob(importUploadStep: Step): Job {
        return jobBuilderFactory
                .get("import")
                .incrementer(RunIdIncrementer())
                .flow(importUploadStep)
                .end()
                .build()
    }

    @Bean(name = ["importUploadStep"])
    fun step1(): Step {
        return stepBuilderFactory.get("importUploadStep")
                .chunk<StagedUpload, Photo>(10)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build()
    }

}

class UploadProcessor(private val repositoryDirectory: String) : ItemProcessor<StagedUpload, Photo> {
    override fun process(uploadedPhoto: StagedUpload): Photo? {
        val sizedPhotos = mutableSetOf<SizedPhoto>()
        val today = LocalDate.now()
        val filenameBase = "$repositoryDirectory/${uploadedPhoto.owner}/$today/${uploadedPhoto.imageId}"
        val filename = "$filenameBase.jpg"
        val dest = File(filename)

        dest.mkdirs()
        File(uploadedPhoto.filename).copyTo(dest, overwrite = true)

        sizedPhotos.add(createThumbnail(PhotoSize.ThumbnailXtraSmall, uploadedPhoto, filenameBase))
        sizedPhotos.add(createThumbnail(PhotoSize.ThumbnailSmall, uploadedPhoto, filenameBase))
        sizedPhotos.add(createThumbnail(PhotoSize.ThumbnailMedium, uploadedPhoto, filenameBase))
        sizedPhotos.add(createThumbnail(PhotoSize.ThumbnailLarge, uploadedPhoto, filenameBase))
        sizedPhotos.add(createThumbnail(PhotoSize.ThumbnailXtraLarge, uploadedPhoto, filenameBase))
        sizedPhotos.add(createThumbnail(PhotoSize.Mobile, uploadedPhoto, filenameBase))
        sizedPhotos.add(createThumbnail(PhotoSize.Desktop, uploadedPhoto, filenameBase))

        return Photo(
                photoId = uploadedPhoto.imageId,
                filenameOriginal = uploadedPhoto.filenameOriginal,
                filename = filename,
                owner = uploadedPhoto.owner,
                contentType = uploadedPhoto.contentType,
                sizedVersions = sizedPhotos)
    }

    private fun createThumbnail(photoSize: PhotoSize, uploadedPhoto: StagedUpload, filenameBase: String): SizedPhoto {
        val scaler = ImageScaler()
        val src = uploadedPhoto.filename
        val dest = "${filenameBase}_$photoSize.jpg"

        val width: Int = when (photoSize) {
            PhotoSize.ThumbnailXtraSmall -> 64
            PhotoSize.ThumbnailSmall -> 128
            PhotoSize.ThumbnailMedium -> 256
            PhotoSize.ThumbnailLarge -> 512
            PhotoSize.ThumbnailXtraLarge -> 1024
            PhotoSize.Mobile -> 640
            PhotoSize.Desktop -> 1920
            PhotoSize.FullSize -> ImageProperties(src).width()
        }
        scaler.scale(src, dest, width)

        val info = ImageProperties(dest)
        return SizedPhoto(
                size = photoSize,
                width = info.width(),
                height = info.height(),
                sizeInByte = 0,
                filename = dest)
    }
}

/**
 * Takes a processed photo including thumbnails and metadata from [UploadProcessor] and stores
 * the physical files into the repository folder and the metadata to the database.
 */
class PhotoWriter(private val photoRepository: PhotoRepository) : ItemWriter<Photo> {

    private val log = LoggerFactory.getLogger(PhotoWriter::class.java)!!

    override fun write(photos: MutableList<out Photo>) {
        photos.forEach { photo ->
            log.info("Digesting photo ${photo.filenameOriginal}")
            photoRepository.save(photo)
        }
    }

}