package de.huepattl.phexpedition.app.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import java.util.*

@Controller("/user")
class UserController(@Autowired val userRepository: UserRepository) {

    @GetMapping("/user")
    fun list(model: Model): String {
        model.addAttribute(
                "users",
                userRepository.findAll())
        return "user/list"
    }

    @GetMapping("/user/{id}")
    fun showSingleUser(model: Model, @PathVariable("id") id: UUID): String {
        val user = userRepository.findById(id)

        model.addAttribute(
                "user",
                user.get())
        return "user/edit"
    }
}