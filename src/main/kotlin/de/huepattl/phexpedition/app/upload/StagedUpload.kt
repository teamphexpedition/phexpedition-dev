package de.huepattl.phexpedition.app.upload

import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Record identifying an uploaded photo. These staged uploads are then subject for later processing into the
 * repository itself.
 *
 * @param imageId Unique identifier of this image uploaded.
 * @param bunchId Grouping identifier of the upload. That is, if many files are uploaded in one row, this bunch groups them together.
 * @param date UTC date of upload.
 * @param filenameOriginal The original file name of the file uploaded (just filename, no path).
 * @param fileName: Absolute file name of the staged file.
 * @param contentType: MIME type of file, e.g. application/jpeg.
 * @param sizeInByte: Size of the file in byte.
 * @param owner User ID of the user uploading and thus owning the file.
 * @param processed When true, the staged photo has been analysed and digested into the repository. Thus, it is safe to delete it.
 *
 * @author blazko@huepattl.de
 * @since 2019-02-28
 */
@Entity
@Table(name = "uploads")
data class StagedUpload(
        @Id
        val imageId: UUID = UUID.randomUUID(),
        val bunchId: UUID,
        val date: Instant = OffsetDateTime.now(ZoneOffset.UTC).toInstant(),
        val filenameOriginal: String,
        val filename: String,
        val contentType: String,
        val sizeInByte: Long,
        val owner: UUID,
        val processed: Boolean = false
)