package de.huepattl.phexpedition.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity


/**
 * Issue with '-Dspring.profiles.active=dev' for development mode.
 */
@SpringBootApplication
@EnableJpaRepositories
@EnableWebSecurity
class PhexpeditionApp
fun main(args: Array<String>) {
    runApplication<PhexpeditionApp>(*args)
}

const val ProfileProd = "prod"
const val ProfileDev = "dev"
const val ProfileTest = "test"
const val ProfileUnitTest = "unit-test"
const val ProfileIntegrationTest = "integration-test"
