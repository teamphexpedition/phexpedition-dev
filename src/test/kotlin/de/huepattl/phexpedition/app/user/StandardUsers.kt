package de.huepattl.phexpedition.app.user

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest
@Transactional
class StandardUsers {

    @Autowired lateinit var userService: UserService

    @Test
    @Rollback(false)
    fun createUser() {
        val user = User(
                name = "user",
                email = "testuser@example.org",
                displayName = "Test user",
                hash = BCryptPasswordEncoder().encode("utest")
        )
        userService.save(user)
    }

    @Test
    @Rollback(false)
    fun createAdmin() {
        val user = User(
                name = "admin",
                email = "testadmin@example.org",
                displayName = "Admin test user",
                hash = BCryptPasswordEncoder().encode("atest")
        )
        userService.save(user)
    }

}