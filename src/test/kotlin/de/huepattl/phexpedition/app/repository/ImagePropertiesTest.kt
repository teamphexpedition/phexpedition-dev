package de.huepattl.phexpedition.app.repository

import org.junit.Assert.assertEquals
import org.junit.Test
import org.springframework.core.io.ClassPathResource

/**
 * Tests for [ImageProperties].
 */
class ImagePropertiesTest {

    @Test
    fun printAllProperties() {
        val pic = ImageProperties(ClassPathResource("example.jpg").file.absolutePath)
        pic.printTags()
    }

    @Test
    fun basics() {
        val pic = ImageProperties(ClassPathResource("example.jpg").file.absolutePath)

        assertEquals(4032, pic.width())
        assertEquals(3024, pic.height())

    }
}