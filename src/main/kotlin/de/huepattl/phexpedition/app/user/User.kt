package de.huepattl.phexpedition.app.user

import org.hibernate.annotations.Type
import java.time.Instant
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "phex_users")
data class User(
        @Id
        @Type(type = "uuid-char") // only required for H2...
        val userId: UUID = UUID.randomUUID(),
        val name: String,
        val validFrom: Instant = Instant.parse("2018-01-01T00:00:00.00Z"),
        val validUntil: Instant = Instant.parse("2099-12-31T23:59:59.99Z"),
        val email: String,
        val displayName: String,
        val language: String = "en",
        val hash: String? = null,
        val avatarId: UUID? = null,
        val active: Boolean = true
)