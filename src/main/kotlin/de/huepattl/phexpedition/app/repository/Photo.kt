package de.huepattl.phexpedition.app.repository

import com.drew.imaging.ImageMetadataReader
import com.drew.imaging.ImageProcessingException
import com.drew.metadata.Metadata
import com.drew.metadata.jpeg.JpegDirectory
import org.slf4j.LoggerFactory
import java.awt.image.BufferedImage
import java.io.File
import java.time.Instant
import java.util.*
import javax.imageio.ImageIO
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToMany

enum class PhotoSize {
    FullSize,
    Desktop,
    Mobile,
    ThumbnailXtraSmall,
    ThumbnailSmall,
    ThumbnailMedium,
    ThumbnailLarge,
    ThumbnailXtraLarge
}

@Entity
data class SizedPhoto(
        @Id
        val sizedId: UUID = UUID.randomUUID(),
        val size: PhotoSize,
        val filename: String,
        val sizeInByte: Long,
        val width: Int,
        val height: Int
)

@Entity
data class Photo(
        @Id
        val photoId: UUID = UUID.randomUUID(),
        val date: Instant = Instant.now(),
        val filenameOriginal: String,
        val filename: String,
        val owner: UUID,
        val contentType: String,

        @OneToMany
        val sizedVersions: Collection<SizedPhoto>
)

/**
 * Tool for resizing pictures, for example used for generating thumbnails.
 */
class ImageScaler {

    private val log = LoggerFactory.getLogger(ImageScaler::class.java)!!


    /**
     * Scales the given image file to the demanded size in pixels.
     */
    fun scale(source: String, destination: String, width: Int = 0, height: Int = 0) {

        val sourceProperties = ImageProperties(source)
        if (width == 0 && height == 0) {
            log.error("When scaling an image, you must at least provide one dimension. Both, width and height were zero")
        }

        var calcHeight: Int = height
        var calcWidth: Int = width
        if (height == 0) {
            val (originalWidth, originalHeight) = Pair(sourceProperties.width(), sourceProperties.height())
            val f = originalWidth / width
            calcHeight = if (f == 0) height else originalHeight / f
            calcWidth = width
        }
        if (width == 0) {
            val (originalWidth, originalHeight) = Pair(sourceProperties.width(), sourceProperties.height())
            val f = originalHeight / height
            calcWidth = if (f == 0) width else originalWidth / f
            calcHeight = height
        }

        log.info("Scaling image $source to $width*$height and storing it to $destination")

        val source = ImageIO.read(File(source))
        val dest = BufferedImage(calcWidth, calcHeight, source.type)

        val g = dest.createGraphics()
        g.drawImage(source, 0, 0, calcWidth, calcHeight, null)
        g.dispose()

        ImageIO.write(dest, "jpg", File(destination))
    }

}

/**
 * Provides a facility to obtain properties of an image file, for example width and height.
 */
class ImageProperties(val imagePath: String) {

    private val metadata: Metadata

    /**
     * Dumps all contained properties to STDOUT including GPS and EXIF information.
     */
    fun printTags() {
        for (directory in metadata.directories) {
            for (tag in directory.tags) {
                System.out.println(tag.toString())
            }
        }
    }

    /**
     * Returns the image width in pixels.
     */
    fun width(): Int {
        return metadata.getFirstDirectoryOfType(JpegDirectory::class.java).imageWidth
    }

    /**
     * Image height in pixels.
     */
    fun height(): Int {
        return metadata.getFirstDirectoryOfType(JpegDirectory::class.java).imageHeight
    }

    init {
        try {
            this.metadata = ImageMetadataReader.readMetadata(File(imagePath))
        } catch (ex: ImageProcessingException) {
            throw IllegalArgumentException(ex)
        }
    }
}