package de.huepattl.phexpedition.app.user

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : CrudRepository<User, UUID> {
    @Query("SELECT u FROM User u WHERE u.name = ?1")
    fun findFirstByName(name: String): User
}