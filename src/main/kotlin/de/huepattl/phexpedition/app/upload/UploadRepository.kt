package de.huepattl.phexpedition.app.upload


import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UploadRepository : CrudRepository<StagedUpload, Int>