package de.huepattl.phexpedition.app.user

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*

@RunWith(BlockJUnit4ClassRunner::class)
class UserSqls {

    @Test
    fun printCreateUserSql() {
        println(createUserSql("user", "phexdemo"))
    }

    private fun createUserSql(user: String, password: String): String {
        val hashed = "{bcrypt}" + BCryptPasswordEncoder().encode(password)
        return "INSERT INTO PHEX_USERS (user_id, active, name, hash) VALUES ('${UUID.randomUUID()}', true, '$user', '$hashed')"
    }

}