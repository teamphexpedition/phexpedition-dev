INSERT INTO PHEX_USERS (user_id, active, name, hash)
SELECT '8dcc392c-8067-4c78-8933-21240bc56378',
       true,
       'demo',
       '{bcrypt}$2a$10$WbdpM6b4oz7QPO1sQQpc2.Cy2Zji1hTIYwmEfxBqumIYvJ602RUfi'
WHERE NOT EXISTS(
    SELECT user_id FROM PHEX_USERS WHERE user_id = '8dcc392c-8067-4c78-8933-21240bc56378'
  );