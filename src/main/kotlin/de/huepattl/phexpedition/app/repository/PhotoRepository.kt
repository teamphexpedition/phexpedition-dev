package de.huepattl.phexpedition.app.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PhotoRepository : CrudRepository<Photo, UUID>