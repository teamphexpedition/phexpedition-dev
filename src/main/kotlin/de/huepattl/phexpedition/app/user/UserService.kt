package de.huepattl.phexpedition.app.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService(
        @Autowired val userRepository: UserRepository) {

    fun save(user: User) {
        userRepository.save(user)
    }
}