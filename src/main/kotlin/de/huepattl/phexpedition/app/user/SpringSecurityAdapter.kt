package de.huepattl.phexpedition.app.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.provisioning.InMemoryUserDetailsManager


@Configuration
@EnableWebSecurity
class SecuredPages : WebSecurityConfigurerAdapter() {
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
                .authorizeRequests()
                .antMatchers("/upload/**").hasRole("USER")
                .antMatchers("/user/**").hasRole("ADMIN")
                .antMatchers("/css/**").permitAll()
                //.authenticated()
                .anyRequest().permitAll()
                .and().formLogin()
                //.loginPage("/login")
                .permitAll()
    }
}

/**
 * Spring Security adapter for production use, will adapt to [UserService].
 */
@Configuration
@Profile("prod", "integration-test")
class SpringSecurityAdapter(@Autowired val userRepository: UserRepository) : UserDetailsService {

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder()
    }

    override fun loadUserByUsername(username: String?): UserDetails {
        val user = userRepository.findFirstByName(username!!)
        println(user)
        return SpringUserDetails(user)
    }

}

/**
 * Spring Security adapter just defining two test users in-memory: "admin" and "user" with passwords "test" each.
 */
@Configuration
@Profile("dev", "test")
class SpringSecurityDevelopmentAdapter {

    @Bean
    fun user(): UserDetailsService {
        val builder = org.springframework.security.core.userdetails.User.withDefaultPasswordEncoder()
        val user = builder
                .username("user")
                .password("test")
                .roles("USER").build()
        val admin = builder
                .username("admin")
                .password("test")
                .roles("USER", "ADMIN").build()
        return InMemoryUserDetailsManager(user, admin)
    }

}

class SpringAuthority : GrantedAuthority {

    override fun getAuthority(): String {
        return "ROLE_USER"
    }

}

private class SpringUserDetails(val user: User) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return mutableSetOf(SpringAuthority())
    }

    override fun isEnabled(): Boolean {
        return user.active
    }

    override fun getUsername(): String {
        return user.name
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String {
        println("Hash: ${user.hash} for ${user.userId}")
        return user.hash!!
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return user.active
    }
}