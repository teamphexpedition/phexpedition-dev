package de.huepattl.phexpedition.app.upload

import de.huepattl.phexpedition.app.ProfileIntegrationTest
import org.junit.Assert
import org.junit.Ignore
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(ProfileIntegrationTest)
@Ignore
class StagedUploadControllerTest {

    @LocalServerPort
    var port: Int = 9000

    var service = TestRestTemplate()

    fun url(): String {
        return "http://localhost:$port/upload"
    }

    /**
     * Request the uploadform, authenticate and check if the word 'Upload' is contained in the response markup.¬
     */
    //@Test
    fun showForm() {
        val e = HttpEntity<String>(HttpHeaders())
        val response = service
                .withBasicAuth("user", "test")
                .exchange(url(), HttpMethod.GET, e, String::class.java)
        Assert.assertEquals(HttpStatus.OK, response.statusCode)
        Assert.assertTrue(response.hasBody())
        Assert.assertTrue(response.body!!.contains("Upload"))
    }

}