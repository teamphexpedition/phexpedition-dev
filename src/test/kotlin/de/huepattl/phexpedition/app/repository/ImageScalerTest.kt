package de.huepattl.phexpedition.app.repository

import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.springframework.core.io.ClassPathResource
import java.util.*


/**
 * Tests for the [ImageScaler] which resizes pictures to the demanded size in pixels.
 */
class ImageScalerTest {

    @Rule
    @JvmField
    var testFolder = TemporaryFolder()

    /**
     * Expecting an exception when trying to load an invalid file format. For example,
     * JPEG would be fine but we provide a plan text file.
     */
    @Test(expected = IllegalArgumentException::class)
    fun illegalFile() {
        val original = ClassPathResource("banner.txt").file
        val scaled = testFolder.newFile("${UUID.randomUUID()}.jpg")
        val resizer = ImageScaler()

        resizer.scale(original.absolutePath, scaled.absolutePath, width = 320, height = 200)
    }

    /**
     * Takes an example JPEG and scales it to 320x200 pixels.
     */
    @Test
    fun scale() {
        // Given
        val original = ClassPathResource("example.jpg").file
        val scaled = testFolder.newFile("${UUID.randomUUID()}.jpg")
        val resizer = ImageScaler()

        // When
        resizer.scale(original.absolutePath, scaled.absolutePath, width = 320, height = 200)

        // Then
        val p = ImageProperties(scaled.absolutePath)
        assertEquals(320, p.width())
        assertEquals(200, p.height())
    }

    /**
     * Scales by only passing the width, which should result in a automatic scaling of the height proportionally.
     * Given that the original test image has a size of 4032x3024 pixels, we expect a height of 1512 pixels when
     * requesting a new width of 2016 pixels (actually, this is a scale of 50%).
     */
    @Test
    fun autoscaleHeight() {
        // Given
        val original = ClassPathResource("example.jpg").file
        val scaled = testFolder.newFile("${UUID.randomUUID()}.jpg")
        val resizer = ImageScaler()

        // When
        resizer.scale(original.absolutePath, scaled.absolutePath, width = 2016)

        // Then
        val p = ImageProperties(scaled.absolutePath)
        assertEquals(2016, p.width())
        assertEquals(1512, p.height())
    }

    /**
     * Scales by only passing the width, which should result in a automatic scaling of the width proportionally.
     * Given that the original test image has a size of 4032x3024 pixels, we expect a width of 2016 pixels when
     * requesting a new height of 1512 pixels (actually, this is a scale of 50%).
     */
    @Test
    fun autoscaleWidth() {
        // Given
        val original = ClassPathResource("example.jpg").file
        val scaled = testFolder.newFile("${UUID.randomUUID()}.jpg")
        val resizer = ImageScaler()

        // When
        resizer.scale(original.absolutePath, scaled.absolutePath, height = 1512)

        // Then
        val p = ImageProperties(scaled.absolutePath)
        assertEquals(2016, p.width())
        assertEquals(1512, p.height())
    }

}