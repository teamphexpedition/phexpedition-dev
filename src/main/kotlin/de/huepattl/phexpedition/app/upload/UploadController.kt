package de.huepattl.phexpedition.app.upload


import de.huepattl.phexpedition.app.user.UserRepository
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.batch.core.Job
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.launch.JobLauncher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.io.File
import java.util.*
import javax.servlet.http.HttpServletResponse


@Controller("/upload")
class UploadController(
        @Autowired val repo: UploadRepository,
        @Value("\${phex.upload.directory}") val stage: String,
        @Autowired val jobLauncher: JobLauncher,
        @Autowired val importJob: Job,
        @Autowired val userRepository: UserRepository) {

    val logger = LoggerFactory.getLogger(UploadController::class.java)!!

    @GetMapping("/upload")
    fun showUploadPage(): String {
        return "upload"
    }

    @GetMapping("/upload/job")
    fun startJob(): String {
        logger.info("Launching import job...")
        jobLauncher.run(importJob, JobParametersBuilder().addString("id", UUID.randomUUID().toString()).toJobParameters())
        return "upload"
    }

    @PostMapping("/upload")
    fun takeUpload(
            @RequestParam("files") files: Array<MultipartFile>,
            redirectAttributes: RedirectAttributes,
            response: HttpServletResponse,
            authentication: Authentication) {

        val bunchId = UUID.randomUUID()
        val userId = userRepository.findFirstByName(authentication.name).userId

        MDC.put("uploadId", "$bunchId")
        MDC.put("userName", "${authentication.name}")

        if (files.size > 1) {
            logger.info("Uploading ${files.size} files...")
        }

        files.forEach {
            val imageId = UUID.randomUUID()
            val filename = "$stage/${authentication.name}_$imageId.jpg"

            val upload = StagedUpload(
                    imageId = imageId,
                    bunchId = bunchId,
                    owner = userId,
                    filenameOriginal = it.originalFilename!!,
                    filename = filename,
                    sizeInByte = it.size,
                    contentType = it.contentType!!)

            logger.info("Staging upload in directory '$stage' for $upload")

            repo.save(upload)
            it.transferTo(File(filename))
        }

        MDC.clear()

        jobLauncher.run(importJob,
                JobParametersBuilder().addString("id", UUID.randomUUID().toString())
                        .toJobParameters())

        response.sendRedirect("/upload")
    }

}